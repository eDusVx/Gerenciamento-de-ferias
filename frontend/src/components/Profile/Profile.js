import axios from 'axios';
import { Logout } from '../../Axios/logout';
import { useState, useRef } from 'react';
import swal from 'sweetalert';

const Profile = () => {
  const [open, setOpen] = useState(false);
  const [changePassword, setChangePassword] = useState(false);
  const menuRef = useRef();
  const imgRef = useRef();
  const username = localStorage.getItem('username');
  const nome = localStorage.getItem('nome');

  const handleLogout = () => {
    //Função para logout
    Logout();
  };

  window.addEventListener("click", (e) => {
    //Função para abrir dropdown menu de perfil
    if(e.target !== menuRef.current && e.target !== imgRef.current){
      setOpen(false);
    }
  });

  const handlePasswordChange = () => {
    //Rotas para troca de senha
    setOpen(false);
    setChangePassword(true);
  };

  const handleFormSubmit = async (e) => {
    //Executando envio do form
    e.preventDefault();
  
    const newPassword = e.target.password.value;
    const confirmNewPassword = e.target.confirmPassword.value;
  
    if (newPassword !== confirmNewPassword) {
      //Verificação de troca de senha
      swal({
        title: "As senhas não são iguais!",
        icon: "warning",
        button: false,
        overlayCSS: {
          backgroundColor: "#000",
          opacity: "0.4",
          zIndex: "9999",
        },
      });
      return;
    }
  
    try {
      await axios.put(`http://127.0.0.1:8000/busca/${username}`, { senha: newPassword });
      setChangePassword(false);
      swal({
        title: "Sua senha foi alterada com sucesso!",
        icon: "success",
        button: "OK",
        overlayCSS: {
          backgroundColor: "#000",
          opacity: "0.4",
          zIndex: "9999",
        },
      });
    } catch (error) {
      console.error(error);
      alert('Ocorreu um erro ao atualizar a senha.');
    }
  };
  

  return (
    <div className="h-screen flex justify-center py-12 z-[2]">
      <div className="absolute top-[25px] right-[12px] bottom-[0px]">
        <div className="flex items-center">
          <img 
            ref={imgRef}
            onClick={()=>setOpen(!open)}
            src="./images/perfil.png" width={45} height={30}
            alt="user"
            className="object-cover cursor-pointer"
          />
          <h1 className="ml-4 text-lg bottom-10">{nome}</h1>
        </div>
        {open && (
        <div ref={menuRef} className="bg-green-700 p-[10px] w-[234px] mt-[10px] shadow-lg absolute -top-300 z-[2]">
          <ul>
              <li onClick={handlePasswordChange} className="p-[10px] text-lg cursor-pointer rounded hover:bg-black hover:text-white hover:rounded" key="AlterarSenha">
                <div>{'Alterar senha'}</div>
              </li>
              <li onClick={handleLogout} className="p-[10px] text-lg cursor-pointer rounded hover:bg-black hover:text-white hover:rounded" key="Logout">
                {'Sair'}
              </li>
          </ul>     
        </div>
        )}
        {changePassword && (
          <form onSubmit={handleFormSubmit} className="bg-green-700 p-4 w-[234px] mt-[10px] shadow-lg absolute -top-300 z-[2] right-0">
            <label htmlFor="password" className="block font-medium text-white mb-1">Senha:</label>
              <input type="password" id="password" name="password" className="border border-gray-400 rounded px-3 py-2 mb-4 w-full" />
            <label htmlFor="confirmPassword" className="block font-medium text-white mb-1">Confirmar senha:</label>
              <input type="password" id="confirmPassword" name="confirmPassword" className="border border-gray-400 rounded px-3 py-2 mb-4 w-full" />
            <div className="grid grid-cols-2 gap-4">
              <button type="submit" className="bg-green-700 text-white py-2 px-4 rounded hover:bg-green-600">Salvar</button>
              <button className="bg-red-700 hover:bg-red-800 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="button" onClick={() => setChangePassword(false)}>Cancelar</button>
            </div>
          </form>
        )}
      </div>
    </div>
  );
};

export default Profile;
