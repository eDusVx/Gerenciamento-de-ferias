import React,{ useState } from 'react'
import { Link } from 'react-router-dom';
import { FaTimes } from 'react-icons/fa'
import { AiOutlineHistory,AiOutlineCalendar,AiOutlineLogout } from 'react-icons/ai'
import { CgProfile } from 'react-icons/cg'
import { Logout } from '../../Axios/logout';
import axios from "axios";
import swal from 'sweetalert';




const SidebarItem = ({ Icon, Text, to, onClick }) => {
  const classes = `flex items-center bg-green-700 text-xl p-[10px] cursor-pointer rounded ml-0 hover:bg-black hover:text-white hover:mr-0`;
  return (
    <Link to={to} className={classes} onClick={onClick}>
      <Icon className="ml-0 mr-[10px]" size="35px"/>
      {Text}
    </Link>
  )
}


const Sidebar = ({ active, onClose }) => {
  const [changePassword, setChangePassword] = useState(false);
  const username = localStorage.getItem('username');

  const handleFormSubmit = async (e) => {
    e.preventDefault();
  
    const newPassword = e.target.password.value;
    const confirmNewPassword = e.target.confirmPassword.value;
  
    if (newPassword !== confirmNewPassword) {
      //Verificação de troca de senha
      swal({
        title: "As senhas não são iguais!",
        icon: "warning",
        button: false,
        overlayCSS: {
          backgroundColor: "#000",
          opacity: "0.4",
          zIndex: "9999",
        },
      });
      return;
    }
  
    try {
      await axios.put(`http://127.0.0.1:8000/busca/${username}`, { senha: newPassword });
      setChangePassword(false);
      swal({
        title: "Sua senha foi alterada com sucesso!",
        icon: "success",
        button: "OK",
        overlayCSS: {
          backgroundColor: "#000",
          opacity: "0.4",
          zIndex: "9999",
        },
      });
    } catch (error) {
      console.error(error);
      alert('Ocorreu um erro ao atualizar a senha.');
    }
  };
  

  const handleLogout = () => {
    //Função para logout
    Logout();
  }

  return (
    <div className={`bg-green-700 fixed h-[100%] top-0 left-0 w-[250px] z-[1] ${active ? "" : "hidden"}`}>
      <FaTimes className="block md:hidden fixed w-[30px] h-[30px] mt-[32px] ml-[32px] cursor-pointer" onClick={onClose} />    
      <div className="mt-[85px]">
        <SidebarItem Icon={AiOutlineHistory} Text="Histórico de solicitações" to="/index_colaborador"/>
        <SidebarItem Icon={AiOutlineCalendar} Text="Solicitações" to="/solicitacoes"/>
        <div className="md:hidden">
          <SidebarItem Icon={CgProfile} Text={'Alterar senha'} onClick={() => {setChangePassword(true)}} />
          <SidebarItem Icon={AiOutlineLogout} Text={'Sair'} onClick={handleLogout}/>
        </div>
        {changePassword && (
          <div className="fixed inset-0 flex items-center justify-center bg-gray-800 bg-opacity-50">
            <div className="bg-green-700 p-10 rounded-lg shadow-lg">
              <form onSubmit={handleFormSubmit}>
                <div className="mb-4">
                  <label htmlFor="senha" className="block text-white text-lg mb-2">Senha</label>
                  <input type="password" className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-green-700" id="password" name="password" placeholder="Senha" />
                </div>
                <div className="mb-6">
                  <label htmlFor="confirmarSenha" className="block text-white text-lg mb-2">Confirmar senha</label>
                  <input type="password" className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-green-700" id="confirmPassword" name="confirmPassword" placeholder="Confirmar senha" />
                </div>
                <div className="flex justify-end">
                  <button className="bg-green-700 hover:bg-green-800 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">Salvar</button>
                  <button className="bg-red-700 hover:bg-red-800 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline ml-4" type="button" onClick={() => setChangePassword(false)}>
                    Cancelar
                  </button>
                </div>
              </form>
            </div>
          </div>
        )}
      </div>
    </div>
  )
}

export default Sidebar
