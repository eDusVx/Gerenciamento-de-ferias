import { useState,useEffect } from 'react';
import './style.css'
import axios from 'axios'
import swal from 'sweetalert';
import { addDays } from 'date-fns';

const SolicitationForm = () => {
  //Componente que gera um formulário de envio de solicitações e faz as validações necessárias
  const [adiantamentoSalario, setAdiantamentoSalario] = useState(false);
  const [outraOpcaoHabilitada, setOutraOpcaoHabilitada] = useState(true);
  const [solicitacoesPendentes, setSolicitacoesPendentes] = useState([]);
  const [solicitacoes, setSolicitacoes] = useState([]);
  const [reloadForm, setReloadForm] = useState(false);
  const [dias, setDias] = useState(5);
  const [dataRetorno, setDataRetorno] = useState("");
  const [dataInicio, setDataInicio] = useState("");
  const mostrarOpcoes = localStorage.getItem('contrato') !== "PJ";

  function handleCheckboxChange(event) {
    //Função que esconde a função 13 caso ja tenha sido solicitada no ano
    if (event.target.value === "sim") {
      setOutraOpcaoHabilitada(false);
    } else {
      setOutraOpcaoHabilitada(true);
    }
    setAdiantamentoSalario(event.target.value === "sim");
  }

  useEffect(() => {
    const username = localStorage.getItem('username');
    axios.get(`http://127.0.0.1:8000/solicitacoes/funcionario/${username}/`)
      .then(response => {
        setSolicitacoesPendentes(response.data.filter(solicitacao => solicitacao.status === "pendente"));
        setSolicitacoes(response.data.filter(solicitacao => solicitacao.status === "aprovado"));
      })
      .catch(error => {
        console.error(error);
      });
  }, []);  

  useEffect(() => {
    if (dataInicio) {
      const novaDataRetorno = addDays(new Date(dataInicio), parseInt(dias));
      setDataRetorno(novaDataRetorno.toISOString().slice(0, 10));
    }
  }, [dataInicio, dias]);

  function handleDataInicioChange(event) {
    setDataInicio(event.target.value);
  }

  function checkDateConflict(dataInicio, dataFim, solicitacoes) {
    //Função que checa se a solicitação atual está em conflito com alguma solicitação existente
    for (let i = 0; i < solicitacoes.length; i++) {
      const solicitacao = solicitacoes[i];
      const inicio = new Date(solicitacao.data_inicio);
      const fim = new Date(solicitacao.data_fim);
      if ((dataInicio >= inicio && dataInicio <= fim) || (dataFim >= inicio && dataFim <= fim)) {
        return true;
      }
    }
    return false;
  }

  function handleSubmit(event) {
    event.preventDefault();
    setReloadForm(true);

    const form = event.target;
    const username = localStorage.getItem('username');

    //Definindo se a opção de 13 salário está disponível ou nao.
    let antecipacaoSalario = false;
    if (!adiantamentoSalario && form.elements.adiantamento13Salario && form.elements.adiantamento13Salario.checked) {
      antecipacaoSalario = true;
    }

    const dataInicio = new Date(form.elements.dataInicio.value);
    const dataRetorno = addDays(new Date(form.elements.dataInicio.value), parseInt(dias));
    form.elements.dataRetorno.value = dataRetorno.toISOString().slice(0, 10);

    //Checando se já existe alguma solicitação pendente
    const solicitacoesPendentesComStatusPendente = solicitacoesPendentes.filter(solicitacao => solicitacao.status === "pendente");
    if (solicitacoesPendentesComStatusPendente.length > 0) {
      swal({
        title: "Erro ao enviar solicitação",
        text: "Você já tem uma solicitação pendente.",
        icon: "error",
        button: false,
        overlayCSS: {
          backgroundColor: "#000",
          opacity: "0.4",
          zIndex: "9999",
        },
      });
      return;
    }

    //Checando se a solicitação está com uma data inválida
    const dataAtual = new Date().toISOString().slice(0, 10);
    if (form.elements.dataInicio.value < dataAtual) {
      swal({
        title: "Erro na solicitação!",
        text: "A data de início não pode ser anterior à data atual.",
        icon: "error",
        button: false,
        overlayCSS: {
          backgroundColor: "#000",
          opacity: "0.4",
          zIndex: "9999",
        },
      });
      return;
    }

    //Checando se a solicitação está com dados conflitantes com alguma existente
    const hasConflict = checkDateConflict(dataInicio, dataRetorno, solicitacoes);
    if (hasConflict) {
      swal({
        title: "Conflito de datas!",
        text: "Você já tem uma solicitação com datas entre este período",
        icon: "error",
        button: false,
        overlayCSS: {
          backgroundColor: "#000",
          opacity: "0.4",
          zIndex: "9999",
        },
      });
      return;
    }
    

    const data = {
      matricula_funcionario: username,
      data_inicio: dataInicio.toISOString().slice(0, 10),
      data_fim: dataRetorno.toISOString().slice(0, 10),
      status: 'pendente',
      data_criacao: new Date().toISOString().slice(0, 10),
      antecipacao_salario: antecipacaoSalario,
      comentarios: form.elements.comentarios.value
    };

    const dias_form = parseInt(form.elements.dias.value);
    const diasDisponiveis = parseInt(localStorage.getItem('diasDisponiveis'));
    const distribuicaoDias = JSON.parse(localStorage.getItem('distribuicaoDias'));

    //Verificando se com a solicitação atual, caso nao acha nenhuma antertior com 15 dias, deixa que hajam solicitações futuras com pelo menos 15 dias.
    let verifica15Dias = false;
    for (let i = 0; i < distribuicaoDias.length; i++) {
      if (distribuicaoDias[i] >= 15) {
        verifica15Dias = true;
        break;
      }
    }
    
    //Função que checa se há dias disponiveis no saldo
    if (dias_form > diasDisponiveis) {
      swal({
        title: "Solicitação não atende aos requisitos!",
        text: `Você não tem saldo de dias suficiente! Seu saldo é de: ${diasDisponiveis} dias`,
        icon: 'error',
        button: false,
        overlayCSS: {
          backgroundColor: '#000',
          opacity: '0.4',
          zIndex: '9999',
        },
      });
      <SolicitationForm />
    }else
    if (!verifica15Dias && dias_form < 15 && (diasDisponiveis - dias_form) < 15) {
      //Checando se há uma solicitação de 15 dias
      swal({
        title: "Solicitação não atende aos requisitos!",
        text: `Você precisa fazer uma solicitação de pelo menos 15 dias.`,
        icon: "error",
        button: false,
        overlayCSS: {
          backgroundColor: "#000",
          opacity: "0.4",
          zIndex: "9999",
        },
      });
      <SolicitationForm />
    }else{
    axios.post('http://127.0.0.1:8000/solicitacoes/', data)
      .then(response => {
        swal({
          title: "Solicitação enviada com sucesso!",
          icon: "success",
          button: "OK",
          overlayCSS: {
            backgroundColor: "#000",
            opacity: "0.4",
            zIndex: "9999",
          },
        });
      })
      .catch(error => {
        console.error(error);
      });
    }
  }

  if (reloadForm) {
    return (
      <>
        <SolicitationForm />
      </>
    );
  }
  return (
    <div className="wrapper bg-gray-300">
      <div className="form">
        <form onSubmit={handleSubmit}>
          <div className="inputfield">
            <label>Dias</label>
            <select name="dias" value={dias} onChange={(e) => setDias(e.target.value)}>
              <option value="5">5 dias</option>
              <option value="10">10 dias</option>
              <option value="15">15 dias</option>
              <option value="20">20 dias</option>
              <option value="30">30 dias</option>
            </select>
          </div>
          <div className="inputfield">
            <label>Data de início</label>
            <input type="date" className="input" name="dataInicio" onChange={handleDataInicioChange} />
          </div>
          <div className="inputfield">
            <label>Data de retorno</label>
            <input type="date" className="input" name="dataRetorno" defaultValue={dataRetorno} />
          </div>
          {mostrarOpcoes && (
            <>
              <div className="inputfield">
                <label>Já adiantou o 13 salário este ano?</label>
                <div>
                  <label>
                    <input className="w-[20px] " type="radio" name="adiantamento" value="sim" checked={adiantamentoSalario} onChange={handleCheckboxChange} />
                    Sim
                  </label>
                  <label>
                    <input type="radio" name="adiantamento" value="nao" checked={!adiantamentoSalario} onChange={handleCheckboxChange} />
                    Não
                  </label>
                </div>
              </div>
              <div className="inputfield">
                <label>Solicitar adiantamento de 13 salário</label>
                <input type="checkbox" className="checkbox" name="adiantamento13Salario" disabled={!outraOpcaoHabilitada} />
              </div>
            </>
          )}
          <div className="inputfield">
            <label>Comentários</label>
            <textarea placeholder="Insira seu comentário" className="textarea" name="comentarios" />
          </div>
          <div className="inputfield">
            <input type="submit" value="Solicitar" className="btn" />
          </div>
        </form>
      </div>
    </div>
  );
}
export default SolicitationForm;

  
