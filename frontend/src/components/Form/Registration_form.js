import { useState } from 'react';
import './style.css'
import axios from 'axios'
import swal from 'sweetalert';

const RegistrationForm = () => {
  //Componente que gera um formulário de cadastro de novos funcionários
   const [reloadForm, setReloadForm] = useState(false);

   const handleSubmit = async (e) => {
    //Função que executa o envio do form
      e.preventDefault();
      setReloadForm(true);
  
      const form = e.target;
      const username = localStorage.getItem('username');
  
      const data = {
        matricula: form.matricula.value,
        nome: form.nome.value,
        contrato: form.contrato.value,
        data_admissao: form.data_admissao.value,
        email_institucional: form.email_constitucional.value,
        gmail: form.gmail.value,
        senha: 'Quero@2023#',
        cargo: form.cargo.value,
        matricula_gestor: username,
      };
  
      axios.post('http://127.0.0.1:8000/funcionario/', data)
      .then(response => {
        console.log(response);
        swal({
          title: "Registro feito com sucesso!",
          icon: "success",
          button: "OK",
          overlayCSS: {
            backgroundColor: "#000",
            opacity: "0.4",
            zIndex: "9999",
          },
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

    if (reloadForm) {
      return (
        <>
          <RegistrationForm />  
        </>
      );
    }
    
    return (
   <>
      <div className="wrapper bg-gray-300">
        <div className="form">
          <form onSubmit={handleSubmit}>
            <div className="inputfield">
              <label htmlFor="nome">Nome completo</label>
              <input type="text" id="nome" name="nome" className="input" placeholder="nome" required />
            </div>
            <div className="inputfield">
              <label htmlFor="matricula">Matrícula</label>
              <input type="text" id="matricula" name="matricula" className="input" placeholder="matrícula" required />
            </div>
            <div className="inputfield">
              <label htmlFor="contrato">Contrato</label>
              <div className="custom_select">
                <select id="contrato" name="contrato" required>
                  <option value="">Selecione um contrato</option>
                  <option value="CLT">CLT</option>
                  <option value="PJ">PJ</option>
                </select>
              </div>
            </div>
            <div className="inputfield">
              <label htmlFor="email_constitucional">E-mail institucional</label>
              <input type="email" id="email_constitucional" name="email_constitucional" className="input" placeholder="email" required />
            </div>
            <div className="inputfield">
              <label htmlFor="gmail">Gmail</label>
              <input type="email" id="gmail" name="gmail" className="input" placeholder="email" required />
            </div>
            <div className="inputfield">
              <label htmlFor="data_admissao">Data de admissão</label>
              <input type="date" id="data_admissao" name="data_admissao" className="input" required />
            </div>
            <div className="inputfield">
              <label htmlFor="cargo">Função</label>
              <input type="text" id="cargo" name="cargo" className="input" placeholder="função" required />
            </div>
            <div className="inputfield">
              <input type="submit" value="Registrar" className="btn" />
            </div>
          </form>
        </div>
      </div>
    </>
    )
}

export default RegistrationForm;
  
