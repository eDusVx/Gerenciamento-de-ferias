import React from "react";

const Footer = () => {
  //Componente que gera um Footer para a página de login
  const year = new Date().getFullYear();

  return <footer className="fixed mb-[15px] bottom-0 left-0 right-0 p-8px text-center text-xs">{`LOJAS QUERO-QUERO S.A. ${year} | todos os direitos reservados`}</footer>;
};

export default Footer;