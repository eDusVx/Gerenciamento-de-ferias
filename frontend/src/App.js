import React from 'react';
import { Routes, Route, BrowserRouter } from 'react-router-dom';
import { IndexColaborador } from './Pages/colaborador/historico_solicitacoes';
import { CadastroColaborador } from './Pages/gestor/cadastro_colaborador';
import { IndexGestor } from './Pages/gestor/index_gestor'
import withAuth from './Axios/auth';
import { Solicitacoes } from './Pages/colaborador/solicitacoes';
import { Colaboradores } from './Pages/gestor/colaboradores'
import { Aprovacoes } from './Pages/gestor/aprovacoes';
import { Login } from './Pages/Login/login';
import { FeriasGestor } from './Pages/gestor/historico_solicitacoes_gestor';
import { SolicitacoesGestor } from './Pages/gestor/solicitacoes_gestor';


function App() {
  const AuthColaboradores = withAuth(Colaboradores, ['gestor_1', 'gestor_2']);//Autenticando página de colaboradores do gestor
  const AuthAprovacoes = withAuth(Aprovacoes,['gestor_1', 'gestor_2']);//Autenticando página de aprovações do gestor
  const AuthIndexGestor = withAuth(IndexGestor,['gestor_1', 'gestor_2']);//Autenticando página de dashboard do gestor
  const AuthCadastroColaborador = withAuth(CadastroColaborador,['gestor_1', 'gestor_2']);//Autenticando página de cadastro de colaboradores do gestor
  const AuthFeriasGestor = withAuth(FeriasGestor,['gestor_1', 'gestor_2']);//Autenticando página histórico de solicitações do gestor
  const AuthSolicitacoesGestor = withAuth(SolicitacoesGestor,['gestor_1', 'gestor_2']);//Autenticando página de solicitar férias do gestor
  const AuthIndexColaborador = withAuth(IndexColaborador,'colaborador');//Autenticando página histórico de solicitações do colaborador
  const AuthSolicitacoes = withAuth(Solicitacoes,'colaborador');//Autenticando página de solicitar férias do colaborador
  
  return (
        //Definindo rotas do sistema com as páginas autenticadas.
        <BrowserRouter>
          <Routes>
            <Route exact path='/' element={<Login />} />
            <Route exact path='/colaboradores' element={<AuthColaboradores />} />
            <Route exact path='/aprovacoes' element={<AuthAprovacoes />} />
            <Route exact path='/index_gestor' element={<AuthIndexGestor />} />
            <Route exact path='/cadastro_colaborador' element={<AuthCadastroColaborador />} />
            <Route exact path="/index_colaborador" element={<AuthIndexColaborador />} />
            <Route exact path='/solicitacoes' element={<AuthSolicitacoes />} />
            <Route exact path='/solicitacoes2' element={<AuthFeriasGestor />} />
            <Route exact path='/historicoSolicitacoes' element={<AuthSolicitacoesGestor />} />
          </Routes>
        </BrowserRouter>
  );
}

export default App;
