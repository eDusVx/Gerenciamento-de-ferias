import React, { useState, useEffect } from 'react'
import HeaderGestor from '../../components/Header/Header_gestor';
import { DonutChart, BarChart } from "@tremor/react";
import axios from 'axios'
import './oi.css'

const valueFormatter = (number) => `${Intl.NumberFormat("us").format(number).toString()} Colaboradores`;
const valueFormatter2 = (number) => `${Intl.NumberFormat("us").format(number).toString()} solicitações`;
const formatter = (number) => `${Intl.NumberFormat("us").format(number).toString()}`;

export const IndexGestor = () => {
  const [colaboradores, setColaboradores] = useState([]);
  const [solicitacoes, setSolicitacoes] = useState([]);
  const [barras, setBarras] = useState([]);
  const username = localStorage.getItem("username");
  const [solicitacoes2, setSolicitacoes2] = useState([]);
  const [anoSelecionado, setAnoSelecionado] = useState(new Date().getFullYear());

useEffect(() => {
  axios
    .get(`http://127.0.0.1:8000/solicitacoes/funcionario/${username}/`)
    .then((response) => {
      setSolicitacoes2(response.data);
    })
    .catch((error) => {
      console.error(error);
    });
}, [username]);

useEffect(() => {
  let diasDisponiveis = 30;
  const distribuicaoDias = solicitacoes2
    .filter((solicitacao) => solicitacao.status === "aprovado")
    .map((solicitacao) => {
      const diffTime = Math.abs(
        new Date(solicitacao.data_fim) - new Date(solicitacao.data_inicio)
      );
      const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
      diasDisponiveis -= diffDays;
      return diffDays;
    });
  // const resultado = { saldo_de_dias: diasDisponiveis, soma: soma };
  localStorage.setItem("diasDisponiveis", diasDisponiveis);
  localStorage.setItem("distribuicaoDias", JSON.stringify(distribuicaoDias));
}, [solicitacoes2]);


  const handleRelatorio = () => {
    //Buscando a rota do backend que gera o relatório xlsx
    fetch(`http://127.0.0.1:8000/dashboard/xlsx/${username}/`)
      .then(response => {
        return response.blob();
      })
      .then(blob => {
        const url = URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", "relatorio.xlsx");
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      })
      .catch(error => {
        console.error(error);
      });
  };
  
  useEffect(() => {
    //Buscando informações dos 3 dashboards
    const fetchData = async () => {
      const requests = [
        fetch(`http://127.0.0.1:8000/dashboard1/${username}/`),
        fetch(`http://127.0.0.1:8000/dashboard2/${username}/`),
        fetch(`http://127.0.0.1:8000/dashboard3/${username}/`)
      ];
  
      const responses = await Promise.all(requests);
      const data = await Promise.all(responses.map(response => response.json()));
  
      const solicitacoes = data[1];
      const statusQuantities = solicitacoes.reduce((acc, cur) => {
        acc.Aprovadas += cur.aprovados;
        acc.Recusadas += cur.recusados;
        acc.Pendentes += cur.pendentes;
        return acc;
      }, { Aprovadas: 0, Recusadas: 0, Pendentes: 0 });
  
      setSolicitacoes([
        { status: "Recusadas", quantidade: statusQuantities.Recusadas },
        { status: "Aprovadas", quantidade: statusQuantities.Aprovadas },
        { status: "Pendentes", quantidade: statusQuantities.Pendentes }
      ]);

      setColaboradores(data[0]);
      setBarras(data[2]);
    };
  
    fetchData();
  }, [username]);
  
  const dadosBarras = barras.filter((item) => item.Ano === anoSelecionado);

  return (
    <>
      <HeaderGestor />
      <div className="w-full">
        <div className="hidden md:block absolute top-0 bottom-0 left-0 h-full w-[250px] bg-gray-100"></div>
        <div className="pl-0 md:pl-[250px]">
          <div className="w-full bg-white md:p-6">
            <div className="cards-container md:grid md:grid-cols-1 md:grid-cols-2 md:gap-6 md:mt-[85px]">
              <div className="md:col-span-2 justify-start">
                <button onClick={handleRelatorio} className="bg-gray-500 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded">
                  Baixar Relatório 
                </button>
              </div>
              <div className="bg-gray-300 rounded-md shadow-md p-6 border-gray-500 border-[0.1px]">
                <h3 className="text-lg font-bold mb-4 text-center">Colaboradores</h3>
                <div className="flex items-center justify-center ">
                  <DonutChart
                    className="w-[250px] h-[250px] md:w-3/4 md:h-[250px] "
                    data={colaboradores}
                    category="quantidade"
                    index="colaboradores"
                    valueFormatter={valueFormatter}
                    colors={["green", "red"]}
                    radius={100}
                  />
                </div>
              </div>
              <div className="bg-gray-300 rounded-md shadow-md p-6 border-gray-500 border-[0.1px]">
                <h3 className="text-lg font-bold mb-4 text-center">Solicitações</h3>
                <div className="flex items-center justify-center">
                  <DonutChart
                    className="w-[250px] h-[250px] md:w-3/4 md:h-[250px]"
                    data={solicitacoes}
                    category="quantidade"
                    index="status"
                    valueFormatter={valueFormatter2}
                    colors={["red", "green", "slate"]}
                    radius={100}
                  />
                </div>
              </div>
              <div className="bg-gray-300 rounded-md shadow-md p-6 col-span-2 border-gray-500 border-[0.1px]">
                <h3 className="text-lg font-bold mb-4">
                  Número de colaboradores em férias por mês
                </h3>
                <div className="flex items-center justify-between">
                  <select
                    className="border border-gray-400 rounded-md py-2 px-3 text-gray-600 font-medium"
                    value={anoSelecionado}
                    onChange={(event) => setAnoSelecionado(parseInt(event.target.value))}
                  >
                    <option value={2023}>2023</option>
                    <option value={2022}>2022</option>
                    <option value={2021}>2021</option>
                  </select>
                </div>
                <div className="flex items-center justify-center">
                <BarChart
                    className="mt-4 h-80"
                    data={dadosBarras}
                    index="Mês"
                    categories={["Colaboradores"]}
                    colors={["red"]}
                    stack={false}
                    valueFormatter={formatter}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};




