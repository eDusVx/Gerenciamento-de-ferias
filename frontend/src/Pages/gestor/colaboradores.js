import React from 'react'
import axios from 'axios';
import { useState,useEffect } from 'react';
import { BiSearch } from 'react-icons/bi';
import { AiOutlineDownload,AiFillEdit } from 'react-icons/ai'
import { TiDelete } from 'react-icons/ti'
import HeaderGestor from '../../components/Header/Header_gestor';
import './style.css'

export const Colaboradores = () => {
  const [searchQuery, setSearchQuery] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [rowsPerPage] = useState(6);
  const [funcionarios, setFuncionarios] = useState([]);

  const headers = {
    "matricula": "Matrícula",
    "name": "Nome",
    "email": "E-mail",
    "major": "Cargo",
    "status": "Status",
    "historico": "Histórico"
  }

  const handleSearchInputChange = (event) => {
    setSearchQuery(event.target.value);
  }

  const handleDownload = async (matricula) => {
    //Gera histórico de solicitações de cada funcionário
    console.log(matricula)
    const response = await axios.get(`http://127.0.0.1:8000/solicitacoes/funcionario/${matricula}/historico`, {
      responseType: 'blob',
    });
    const url = window.URL.createObjectURL(new Blob([response.data]));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', 'historico.xlsx');
    document.body.appendChild(link);
    link.click();
  };

  useEffect(() => {
    //Busca as informações dos funcionários ligados ao gestor
  async function fetchData() {
    const username = localStorage.getItem('username');
    const response = await axios.get(`http://127.0.0.1:8000/funcionario/${username}/`);

    const funcionariosLocalStorage = JSON.parse(localStorage.getItem('funcionarios')) || [];

    const funcionariosCompletos = response.data.map((funcionario) => {
      const funcionarioLocalStorage = funcionariosLocalStorage.find((f) => f.matricula === funcionario.matricula);
      if (funcionarioLocalStorage) {
        return {
          ...funcionario,
          ultimasFerias: funcionarioLocalStorage.ultimasFerias,
        };
      } else {
        return funcionario;
      }
    });

    setFuncionarios(funcionariosCompletos);
  }

  fetchData();
}, []);


const content = funcionarios.map((funcionario) => {
  const funcionarios = JSON.parse(localStorage.getItem('funcionarios')).find(c => c.matricula === funcionario.matricula);
  const ferias = funcionarios && funcionarios.ferias === 'SIM' ? 'Férias' : 'Ativo';

  return {
    matricula: funcionario.matricula,
    name: funcionario.nome,
    email: funcionario.email_institucional,
    major: funcionario.cargo,
    status: ferias,
    ultimasFerias: funcionario.ultimasFerias 
  };
}).sort((a, b) => {
  if (a.status === "Ativo" && b.status !== "Ativo") {
    return -1;
  } else if (a.status !== "Ativo" && b.status === "Ativo") {
    return 1;
  } else {
    return 0;
  }
});

//Setando funcionários em uma tablea
const filteredContent = content.filter((item) => {
  return (
    item.matricula.toLowerCase().includes(searchQuery.toLowerCase()) ||
    item.name.toLowerCase().includes(searchQuery.toLowerCase()) ||
    item.email.toLowerCase().includes(searchQuery.toLowerCase()) ||
    item.major.toLowerCase().includes(searchQuery.toLowerCase()) ||
    item.status.toLowerCase().includes(searchQuery.toLowerCase())
  );
}).map((item) => {
  //Sinaliza caso o funcionário esteja a 1 mes de acumular férias
  const className = item.ultimasFerias === 23 ? "highlight" : "";
  return { ...item, className };
});

  const indexOfLastRow = currentPage * rowsPerPage;
  const indexOfFirstRow = indexOfLastRow - rowsPerPage;
  const currentRows = filteredContent.slice(indexOfFirstRow, indexOfLastRow);

  const totalPages = Math.ceil(filteredContent.length / rowsPerPage);

  const handlePageChange = (page) => {
    setCurrentPage(page);
  }

  return (
    <>
      <HeaderGestor/>
      
      <div className="wrapper bg-gray-300">
      <div className="index md:hidden flex items-center mb-4">
        <span className="red-tag inline-block w-2.5 h-2.5 mr-2 bg-red-500 rounded-full"></span>
        <p>Funcionários a 1 mês de acumular período de férias</p>
      </div>
        <div className="search-container">
          <input type="text" placeholder="Search" value={searchQuery} onChange={handleSearchInputChange} />
          <BiSearch className="search-icon" />
        </div>
        <table>
          <thead>
            <tr>
              {Object.values(headers).map((header) => (
                <th key={header} scope="col">{header}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {currentRows.map((item) => (
              <tr key={item.matricula} className={item.className}>
                {Object.keys(headers).map((key) => (
                  <td
                  key={key}
                  data-label={headers[key]}
                  className={
                    key === "status"
                      ? item.status === "Ativo"
                        ? "active-status"
                        : item.status === "Férias"
                        ? "vacation-status"
                        : ""
                      : key === "ultimasFerias" && item.ultimasFerias === 2
                      ? "red-cell"
                      : ""
                  }
                >
                  {key === "edit" ? (
                    <button className="edit">
                      <AiFillEdit size={20} />
                    </button>
                  ) : key === "delete" ? (
                    <button className="delete">
                      <TiDelete size={20} />
                    </button>
                  ) : key === "historico" ? (
                    <button className="historico" onClick={() => handleDownload(item.matricula)}>
                      <AiOutlineDownload size={20} />
                    </button>
                  ) : (
                    item[key]
                  )}
                </td>
              ))}
            </tr>
          ))}
          </tbody>
        </table>
        <div className="pagination">
          <button
            onClick={() => handlePageChange(currentPage - 1)}
            disabled={currentPage === 1}
          >
            Previous
          </button>
          {Array.from({ length: totalPages }, (v, i) => i + 1).map((page) => (
            <button
              key={page}
              className={currentPage === page ? "active" : ""}
              onClick={() => handlePageChange(page)}
            >
              {page}
            </button>
          ))}
          <button
            onClick={() => handlePageChange(currentPage + 1)}
            disabled={currentPage === totalPages}
          >
            Next
          </button>
        </div>
        <div className="index hidden md:block md:flex items-center mb-4">
          <span className="red-tag inline-block w-2.5 h-2.5 mr-2 bg-red-500 rounded-full"></span>
          <p>Funcionários a 1 mês de acumular período de férias</p>
        </div>
      </div>
    </>
  );
}