import React, { useEffect } from 'react';
import SolicitationForm from '../../components/Form/Solicitation_form';
import HeaderGestor from '../../components/Header/Header_gestor';
import swal from 'sweetalert';
import { useNavigate } from 'react-router-dom';

export const SolicitacoesGestor = () => {
  const navigate = useNavigate();

  useEffect(() => {
    //Verificação de 1 ano de empresa completo, caso nao passe o usuário nao pode acessar a página de solicitar agendamentos e é redirecionado a pagina anterior
    const tempoEmpresa = localStorage.getItem('tempo_empresa');
    if (tempoEmpresa === 'Não') {
      swal({
        title: 'Você ainda não completou 1 ano de empresa',
        icon: 'warning',
        button: false,
        overlayCSS: {
          backgroundColor: '#000',
          opacity: '0.4',
          zIndex: '9999',
        },
      }).then(() => {
        navigate(-1);
      });
    }
  }, [navigate]);

  return (
    <>
      <HeaderGestor />
      <SolicitationForm />
    </>
  );
};
