import React from 'react'
import { useState,useEffect } from 'react';
import { BiSearch } from 'react-icons/bi';
import { AiOutlineCheck } from 'react-icons/ai'
import { BiCommentDetail } from 'react-icons/bi'
import { TiDelete } from 'react-icons/ti'
import HeaderGestor from '../../components/Header/Header_gestor';
import './style.css'
import axios from 'axios'
import moment from "moment";
import swal from 'sweetalert';

export const Aprovacoes = () => {
  const username = localStorage.getItem("username");
  const [data, setData] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [rowsPerPage] = useState(5);
  const [texto, setTexto] = useState("");
  const [textoVisivel, setTextoVisivel] = useState(false);
  const [textoComentario, setTextoComentario] = useState("Boas férias!");
  const [textoComentario2, setTextoComentario2] = useState("Férias Recusadas!");
  
  const handleTextoChange = (event) => {
    setTextoComentario(event.target.value);
    setTextoComentario2(event.target.value);
  };
  
  const handleSubmit = (event) => {
    event.preventDefault();
    setTexto("");
    setTextoVisivel(false);
  }

  const headers = {
    "nome_funcionario": "Nome",
    "cargo_funcionario": "Cargo",
    "periodo": "Período",
    "numero_dias": "Número de dias",
    "comentarios": "Comentários",
    "edit": "Aprovar",
    "delete": "Recusar",
    "comentario": "Adicionar comentário",
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          `http://127.0.0.1:8000/solicitacoes/gestor/${username}/`
        );
        const filteredData = response.data
          .filter((item) => item.status === "pendente")
          .map((item) => ({
            ...item,
            periodo: `${item.data_inicio}~${item.data_fim}`,
            numero_dias: moment(item.data_fim).diff(
              moment(item.data_inicio),
              "days"
            ),
          }));
  
        setData(filteredData);
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }, []);
  

  const handleSearchInputChange = (event) => {
    setSearchQuery(event.target.value);
  };

  const filteredContent = data.filter((item) => {
    return (
      item.nome_funcionario.toLowerCase().includes(searchQuery.toLowerCase()) ||
      item.cargo_funcionario.toLowerCase().includes(searchQuery.toLowerCase()) ||
      item.periodo.toLowerCase().includes(searchQuery.toLowerCase()) ||
      item.status.toLowerCase().includes(searchQuery.toLowerCase()) ||
      item.comentarios.toLowerCase().includes(searchQuery.toLowerCase())
    );
  });

  const indexOfLastRow = currentPage * rowsPerPage;
  const indexOfFirstRow = indexOfLastRow - rowsPerPage;
  const currentRows = filteredContent.slice(indexOfFirstRow, indexOfLastRow);

  const totalPages = Math.ceil(filteredContent.length / rowsPerPage);

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const handleApproveClick = async (id) => {
    //Função que aprova uma solicitação
    try {
      const response = await axios.put(`http://127.0.0.1:8000/solicitacoes/${id}/`, {
        nome_funcionario: data.find((item) => item.id === id).nome_funcionario,
        cargo_funcionario: data.find((item) => item.id === id).cargo_funcionario,
        data_inicio: data.find((item) => item.id === id).data_inicio,
        data_fim: data.find((item) => item.id === id).data_fim,
        status: "aprovado", 
        data_criacao: data.find((item) => item.id === id).data_criacao,
        antecipacao_salario: data.find((item) => item.id === id).antecipacao_salario,
        comentarios: textoComentario,
        matricula_funcionario: data.find((item) => item.id === id).matricula_funcionario,
        notificacoes: data.find((item) => item.id === id).notificacoes,
      });
      const updatedData = data.map((item) =>
        item.id === id ? { ...item, status: response.data.status } : item
      );
      setData(updatedData);
      swal({
        title: "Solicitação Aprovada!",
        icon: "success",
        button: "OK",
        overlayCSS: {
          backgroundColor: "#000",
          opacity: "0.4",
          zIndex: "9999",
        },
      });
      setTimeout(() => {
      window.location.reload();
      }, 1000);
    } catch (error) {
      console.log(error);
    }
  };

  const handleDesaAproveClick = async (id) => {
    //Função que recusa uma solicitação
    try {
      const response = await axios.put(`http://127.0.0.1:8000/solicitacoes/${id}/`, {
        nome_funcionario: data.find((item) => item.id === id).nome_funcionario,
        cargo_funcionario: data.find((item) => item.id === id).cargo_funcionario,
        data_inicio: data.find((item) => item.id === id).data_inicio,
        data_fim: data.find((item) => item.id === id).data_fim,
        status: "recusado",
        data_criacao: data.find((item) => item.id === id).data_criacao,
        antecipacao_salario: data.find((item) => item.id === id).antecipacao_salario,
        comentarios: textoComentario2,
        matricula_funcionario: data.find((item) => item.id === id).matricula_funcionario,
        notificacoes: data.find((item) => item.id === id).notificacoes,
      });
      const updatedData = data.map((item) =>
        item.id === id ? { ...item, status: response.data.status } : item
      );
      setData(updatedData);
      swal({
        title: "Solicitação Reprovada!",
        icon: "success",
        button: "OK",
        overlayCSS: {
          backgroundColor: "#000",
          opacity: "0.4",
          zIndex: "9999",
        },
      });
      setTimeout(() => {
      window.location.reload();
      }, 1000);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <HeaderGestor/>
      <div className="wrapper bg-gray-300">
        <div className="search-container">
          <input type="text" placeholder="Search" value={searchQuery} onChange={handleSearchInputChange} />
          <BiSearch className="search-icon" />
        </div>
        <table>
          <thead>
            <tr>
              {Object.values(headers).map((header) => (
                <th key={header} scope="col">{header}</th>
              ))}
            </tr>
          </thead>
          <tbody>
          {currentRows.map((item) => (
            <tr key={item.id}>
              {Object.keys(headers).map((key) => (
                <td
                  key={key}
                  data-label={headers[key]}
                  className={
                    key === "status"
                      ? item.status === "Ativo"
                        ? "active-status"
                        : item.status === "Férias"
                        ? "vacation-status"
                        : ""
                      : ""
                  }
                >
                  {key === "edit" ? (
                    <button className="edit" onClick={() => handleApproveClick(item.id)}><AiOutlineCheck size={20}/></button>
                  ) : key === "delete" ? (
                    <button className="delete" onClick={() => handleDesaAproveClick(item.id)}><TiDelete size={20}/></button>
                  ) : key === "comentario" ? (
                    <button className="historico" onClick={() => setTextoVisivel(true)}><BiCommentDetail size={20}/></button>
                  ): (
                    item[key]
                  )}
                  {textoVisivel && (
                      <div className="comment-form-wrapper">
                        <form className="comment-form" onSubmit={handleSubmit}>
                          <label>
                            Escreva aqui:
                            <textarea value={textoComentario} onChange={handleTextoChange} />
                          </label>
                          <button type="submit">Salvar</button>
                        </form>
                      </div>
                    )}
                </td>
              ))}
            </tr>
          ))}
          </tbody>
        </table>
        <div className="pagination">
          <button
            onClick={() => handlePageChange(currentPage - 1)}
            disabled={currentPage === 1}
          >
            Previous
          </button>
          {Array.from({ length: totalPages }, (v, i) => i + 1).map((page) => (
            <button
              key={page}
              className={currentPage === page ? "active" : ""}
              onClick={() => handlePageChange(page)}
            >
              {page}
            </button>
          ))}
          <button
            onClick={() => handlePageChange(currentPage + 1)}
            disabled={currentPage === totalPages}
          >
            Next
          </button>
        </div>
      </div>
    </>
  );
}