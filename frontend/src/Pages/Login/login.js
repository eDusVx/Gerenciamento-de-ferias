import { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import Footer from '../../components/Footer/Footer';
import jwt_decode from 'jwt-decode';

export const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(true);
  const navigate = useNavigate();

  const handleLogin = async e => {
    //Função que executa o login do usuário
    e.preventDefault();
  
    const user = {
      username: username,
      password: password
    };
  
    try {
      //Gerando token JWT
      const {data} = await axios.post('http://localhost:8000/token/', user , {
        headers: {
          'Content-Type': 'application/json'
        },
        withCredentials: true
      });
  
      localStorage.clear();
      localStorage.setItem('access_token', data.access);
      localStorage.setItem('refresh_token', data.refresh);
      localStorage.setItem('username', username);
      axios.defaults.headers.common['Authorization'] = `Bearer ${data['access']}`;
      window.location.reload()
    } catch (error) {
      setError('Usuário ou senha incorretos');
    }
  }
  
  useEffect(() => {
    //Setando período concessivo e aquisitivo de acordo com a data de entrada na empresa(para usuários que nao possuem nenhuma solicitação ativa)
    async function fetchData() {
      const accessToken = localStorage.getItem('access_token');
      if (!accessToken) {
        return;
      }
      const decodedToken = jwt_decode(accessToken);
      localStorage.setItem('user',decodedToken.group)
      
      const response = await axios.get(`http://127.0.0.1:8000/busca/${localStorage.getItem('username')}/`);
      localStorage.setItem('contrato', response.data.contrato);
      localStorage.setItem('nome', response.data.nome)
      
      const dataAdmissao = new Date(response.data.data_admissao);
      const tempoEmpresa = (Date.now() - dataAdmissao.getTime()) / (1000 * 60 * 60 * 24 * 365);
      localStorage.setItem('tempo_empresa', tempoEmpresa >= 1 ? 'Sim' : 'Não');
  
      const periodoAquisitivo = {
        inicio: dataAdmissao.toISOString().substr(0, 10),
        fim: new Date(dataAdmissao.getFullYear() + 1, dataAdmissao.getMonth(), dataAdmissao.getDate()).toISOString().substr(0, 10)
      };
  
      const periodoConcessivo = {
        inicio: new Date(dataAdmissao.getFullYear() + 1, dataAdmissao.getMonth(), dataAdmissao.getDate()).toISOString().substr(0, 10),
        fim: new Date(dataAdmissao.getFullYear() + 2, dataAdmissao.getMonth(), dataAdmissao.getDate()).toISOString().substr(0, 10)
      };
  
      const periodos = [periodoAquisitivo, periodoConcessivo];
      localStorage.setItem('periodo', JSON.stringify(periodos));
      setLoading(false);
    }
    fetchData();
  }, []);
  const periodo = localStorage.getItem('periodo')

  useEffect(() => {
    //Redirecionando para a página correta de acordo com o usuário logado
    const user = localStorage.getItem('user')
    if (!loading && periodo !== null) {
      switch (user) {
        case 'gestor_1':
          navigate('/index_gestor');
          break;
        case 'gestor_2':
          navigate('/index_gestor');
          break;
        case 'colaborador':
          navigate('/index_colaborador');
          break;
        default:
          break;
      }
    }
  }, [periodo, navigate, loading]);
  
  return (
    <>
      <div className="Logo items-center flex flex-row flex-wrap justify-center mt-[15%]">
        <img src="./images/queroquero.png" alt="QQlogo" width={350}/>
      </div>
      <form onSubmit={handleLogin}>
        <div className="container-login w-[300px] p-[30px] m-auto flex flex-row flex-wrap rounded-md justify-center">
          <input
            className="text-center mb-[20px] p-[8px] h-[30px] rounded shadow bg-slate-300"
            type="text"
            placeholder="Matrícula"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
          <input
            className="text-center mb-[20px] p-[8px] h-[30px] rounded shadow bg-slate-300"
            type="password"
            placeholder="*********"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          {error && <p className="text-red-500">{error}</p>}
          <button className="justify-center rounded w-[100px] h-[30px] m-auto bg-slate-500" id="entrar">
            Entrar
          </button>
        </div>
      </form>
      <Footer />
    </>
  );
};


