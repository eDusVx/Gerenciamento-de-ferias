import React from 'react'
import { useState, useEffect } from 'react';
import { BiSearch } from 'react-icons/bi';
import Header from '../../components/Header/Header';
import './style.css'
import axios from 'axios'
import moment from 'moment';
import swal from 'sweetalert';
import { Verificacoes } from '../../Axios/verificações';
const username = localStorage.getItem('username');

export const IndexColaborador = () => {
  const [solicitacoes, setSolicitacoes] = useState([]);
  const username = localStorage.getItem("username");
  const [solicitacoes2, setSolicitacoes2] = useState([]);

  useEffect(() => {
    axios
      .get(`http://127.0.0.1:8000/solicitacoes/funcionario/${username}/`)
      .then((response) => {
        setSolicitacoes2(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [username]);

  useEffect(() => {
    //Setando os dias disponíveis e os periodos aquisitivo e concessivo
    let diasDisponiveis = 30;
    const hoje = new Date();
    const periodoAquisitivo = JSON.parse(localStorage.getItem('periodo'))[0];
    const inicioPeriodoAquisitivo = new Date(periodoAquisitivo.inicio);
    const fimPeriodoAquisitivo = new Date(periodoAquisitivo.fim);

    if (hoje >= inicioPeriodoAquisitivo && hoje <= fimPeriodoAquisitivo) {
      diasDisponiveis = 0;
    }

    const periodo = JSON.parse(localStorage.getItem('periodo'));
    const periodoConcessivo = periodo[1];
    const distribuicaoDias = solicitacoes2
      .filter((solicitacao) => {
        const inicioPeriodo = Date.parse(periodoConcessivo.inicio);
        const fimPeriodo = Date.parse(periodoConcessivo.fim);
        const inicioSolicitacao = Date.parse(solicitacao.data_inicio);
        const fimSolicitacao = Date.parse(solicitacao.data_fim);
        return solicitacao.status === "aprovado" && inicioSolicitacao >= inicioPeriodo && fimSolicitacao <= fimPeriodo;
      })
      .map((solicitacao) => {
        const diffTime = Math.abs(
          new Date(solicitacao.data_fim) - new Date(solicitacao.data_inicio)
        );
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        diasDisponiveis -= diffDays;
        return diffDays;
      });
  
    if (diasDisponiveis === 0) {
      const ultimaSolicitacaoAprovada = solicitacoes2
        .filter(solicitacao => solicitacao.status === "aprovado")
        .sort((a, b) => Date.parse(b.data_criacao) - Date.parse(a.data_criacao))[0];
      
      if (ultimaSolicitacaoAprovada && ultimaSolicitacaoAprovada.data_fim) {
        const inicioaquisitivo = moment(ultimaSolicitacaoAprovada.data_criacao, "YYYY-MM-DD").format("YYYY-MM-DD");
        const fimaquisitivo = moment(inicioaquisitivo, "YYYY-MM-DD").add(1, "years").format("YYYY-MM-DD");
        const inicioPeriodoConcessivo = moment(fimaquisitivo, "YYYY-MM-DD").add(1, "days").format("YYYY-MM-DD");
        const fimPeriodoConcessivo = moment(inicioPeriodoConcessivo, "YYYY-MM-DD").add(1, "years").format("YYYY-MM-DD");
        const periodoAquisitivo = [{ inicio: inicioaquisitivo, fim: fimaquisitivo }, { inicio: inicioPeriodoConcessivo, fim: fimPeriodoConcessivo }];
        localStorage.setItem('periodo', JSON.stringify(periodoAquisitivo));
      } else {
      }
    }
    localStorage.setItem("diasDisponiveis", diasDisponiveis);
    localStorage.setItem("distribuicaoDias", JSON.stringify(distribuicaoDias));
  }, [solicitacoes2]);

  useEffect(() => {
    //Buscando as solicitações do usuario
    async function fetchData() {
      const { data } = await axios.get(`http://127.0.0.1:8000/solicitacoes/funcionario/${username}/`);
      setSolicitacoes(data);
      const updatedSolicitacoes = data.map((item) => {
        const dataInicio = moment(item.data_inicio, 'YYYY-MM-DD');
        const dataCriacao = moment(item.data_criacao, "YYYY-MM-DD").format("DD/MM/YYYY");
        const dataFim = moment(item.data_fim, 'YYYY-MM-DD');
        const numeroDias = dataFim.diff(dataInicio, 'days');
        const periodo = `${dataInicio.format('DD/MM/YYYY')} - ${dataFim.format('DD/MM/YYYY')}`;

        return {
          ...item,
          data_criacao:dataCriacao,
          numero_dias: numeroDias,
          periodo: periodo,
          antecipacao_salario: item.antecipacao_salario ? "Solicitada" : "Não solicitada",
        };
      });

      setSolicitacoes(updatedSolicitacoes);
    }

    fetchData();
  }, [username]);


  const headers = {
    "data_criacao": "Data da solicitção",
    "periodo": "Periodo",
    "numero_dias": "Número de dias",
    "status": "Status",
    "comentarios": "comentários",
  };
  
  const [searchQuery, setSearchQuery] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [rowsPerPage] = useState(5);
  
  const handleSearchInputChange = (event) => {
    //Pesquisa na tabela
    setSearchQuery(event.target.value);
  };
  
  // Verifica se localStorage contrato é igual a 'CLT'
  if (localStorage.getItem("contrato") === "CLT") {
    headers.antecipacao_salario = "antecipacao do 13";
  } else {
    delete headers.antecipacao_salario;
  }
  
  //Setando as informações na tabela e ordenando por data
  const filteredContent = solicitacoes
  .sort((a, b) => moment(a.data_criacao, "YYYY-MM-DD").diff(moment(b.data_criacao, "YYYY-MM-DD")))
  .filter((item) => {
    const antecipacaoSalario =
      headers.antecipacao_salario &&
      item.antecipacao_salario.toLowerCase().includes(searchQuery.toLowerCase());
    return (
      item.data_criacao.toLowerCase().includes(searchQuery.toLowerCase()) ||
      item.periodo.toLowerCase().includes(searchQuery.toLowerCase()) ||
      item.numero_dias.toString().toLowerCase().includes(searchQuery.toLowerCase()) ||
      item.status.toLowerCase().includes(searchQuery.toLowerCase()) ||
      item.comentarios.toLowerCase().includes(searchQuery.toLowerCase()) ||
      antecipacaoSalario
    );
  });

  const sortedSolicitacoes = solicitacoes.sort((a, b) => {
  return moment(b.data_criacao, "YYYY-MM-DD").valueOf() - moment(a.data_criacao, "YYYY-MM-DD").valueOf();
});
  const indexOfLastRow = currentPage * rowsPerPage;
  const indexOfFirstRow = indexOfLastRow - rowsPerPage;
  const currentRows = sortedSolicitacoes.slice(indexOfFirstRow, indexOfLastRow);

  const totalPages = Math.ceil(filteredContent.length / rowsPerPage);

  const handlePageChange = (page) => {
    setCurrentPage(page);
  }
  Verificacoes()//Chamando a função que verifica se o usuário está acumulando férias
  const funcionarios = JSON.parse(localStorage.getItem('funcionarios'));
  const funcionario = funcionarios ? funcionarios.find((f) => f.matricula === username) : null;
  const periodo = JSON.parse(localStorage.getItem('periodo'));
  const aquisitivoInicio = moment(periodo[0].inicio).format('DD/MM/YYYY');
  const aquisitivoFim = moment(periodo[0].fim).format('DD/MM/YYYY');
  const concessivoInicio = moment(periodo[1].inicio).format('DD/MM/YYYY');
  const concessivoFim = moment(periodo[1].fim).format('DD/MM/YYYY');

  return (
    <>
      <Header />
      <div className="md:hidden">
        <div className="bg-gray-300 absolute transform top-[85px] w-full right-[0px] h-auto z-[-1]">
          <div className="flex flex-col justify-center items-center p-2">
            <p className="text-lg text-center font-bold text-black mb-2">Saldo de dias disponíveis: {localStorage.getItem('diasDisponiveis')}</p>
            <p className="text-lg text-center font-bold text-black mb-2">Período aquisitivo: {aquisitivoInicio} - {aquisitivoFim}</p>
            <p className="text-lg text-center font-bold text-black mb-2">Período concessivo: {concessivoInicio} - {concessivoFim}</p>
          </div>
        </div>
      </div>
      <div className="hidden md:block bg-gray-300 absolute transform top-[170px] w-header right-[0px] h-[170px] z-[-1]">
        <div className="flex flex-row justify-center items-center h-full">
          <div className="flex-1 flex flex-col justify-center items-center">
            <p className="text-xl text-center font-bold text-black mb-2">Saldo de dias disponíveis: {localStorage.getItem('diasDisponiveis')}</p>
          </div>
          <div className="flex-1 flex flex-col justify-center items-center">
            <p className="text-xl text-center font-bold text-black mb-2">Período aquisitivo: {aquisitivoInicio} - {aquisitivoFim}</p>
          </div>
          <div className="flex-1 flex flex-col justify-center items-center">
            <p className="text-xl text-center font-bold text-black mb-2">Período concessivo: {concessivoInicio} - {concessivoFim}</p>
          </div>
        </div>
      </div>
      <div className="wrapper2 bg-gray-300">
        {funcionario && funcionario.ultimasFerias >= 23 && (
          <div className="index flex items-center mb-4">
            <span className="red-tag inline-block w-2.5 h-2.5 mr-2 bg-red-500 rounded-full"></span>
            <p>Suas férias estão a 1 mês de acumular!</p>
          </div>
        )}
        <div className="search-container">
          <input type="text" placeholder="Search" value={searchQuery} onChange={handleSearchInputChange} />
          <BiSearch className="search-icon" />
        </div>
        <table>
          <thead>
            <tr>
              {Object.values(headers).map((header) => (
                <th key={header} scope="col">{header}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {currentRows.map((item, index) => (
              <tr key={index}>
                {Object.keys(headers).map((key) => (
                  <td
                    key={key}
                    data-label={headers[key]}
                    className={
                      key === "status"
                        ? item.status === "aprovado"
                          ? "active-status"
                          : item.status === "recusado"
                            ? "vacation-status"
                            : item.status === "pendente"
                              ? "pending-status"
                              : ""
                        : ""
                    }
                  >
                    {key === "detalhes" ? (
                      <button className="detalhes">Ver detalhes</button>
                    ) : (
                      item[key]
                    )}
                  </td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
        <div className="pagination">
          <button
            onClick={() => handlePageChange(currentPage - 1)}
            disabled={currentPage === 1}
          >
            Anterior
          </button>
          {Array.from({ length: totalPages }, (v, i) => i + 1).map((page) => (
            <button
              key={page}
              className={currentPage === page ? "active" : ""}
              onClick={() => handlePageChange(page)}
            >
              {page}
            </button>
          ))}
          <button
            onClick={() => handlePageChange(currentPage + 1)}
            disabled={currentPage === totalPages}
          >
            Próxima
          </button>
        </div>
      </div>
    </>
  );
}

async function Verifica() {
  //Função que retorna um aviso de que passaram 11 meses desde o último periodo de férias
  await Verificacoes();
  const funcionarios = JSON.parse(localStorage.getItem('funcionarios'));
  const funcionario = funcionarios.find((f) => f.matricula === username);
  if (funcionario && funcionario.ultimasFerias === 11) {
    swal({
      title: 'Se passaram 11 meses desde seu último periodo de férias',
      icon: 'warning',
      button: false,
      overlayCSS: {
        backgroundColor: '#000',
        opacity: '0.4',
        zIndex: '9999',
      },
    }).then(() => {
    });
  }
}
Verifica()
