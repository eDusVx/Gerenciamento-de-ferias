import React, { useEffect } from "react";
import { useNavigate } from 'react-router-dom';
import jwt_decode from 'jwt-decode';

const withAuth = (WrappedComponent, allowedGroups) => {
  //Função que autentica as páginas com o token JWT
  const AuthCheck = () => {
    const token = localStorage.getItem("access_token");
    const navigate = useNavigate();

    useEffect(() => {
      if (!token) {
        navigate("/");
      } else {
        try {
          const decodedToken = jwt_decode(token);
          if (!allowedGroups.includes(decodedToken.group)) {
            navigate(-1);
          }
        } catch (error) {
            navigate(-1);
        }
      }
    }, [navigate, token, allowedGroups]);

    return <WrappedComponent />;
  };

  return AuthCheck;
};

export default withAuth;
