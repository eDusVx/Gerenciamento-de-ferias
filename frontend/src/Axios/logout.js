import axios from "axios";

export const Logout = async () => {
  //Função que executa logout de usuário
  const refresh_token = localStorage.getItem('refresh_token');

  const new_access_token = await axios.post('http://localhost:8000/token/refresh/', {refresh: refresh_token})
    .then(response => {
      localStorage.setItem('access_token', response.data.access);
      return response.data.access;
    })
    .catch(error => {
      console.log(error);
    });

  axios.post('http://localhost:8000/logout/', {"refresh_token": refresh_token}, {
    headers: {
      Authorization: `Bearer ${new_access_token}`
    }
  })
  .then(response => {
    console.log(response.data);
    window.location.href = '/'
    localStorage.clear();
  })
  .catch(error => {
    console.log(error);
  });
}
