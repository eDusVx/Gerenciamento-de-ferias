import axios from "axios";

export const Verificacoes = async () => {
  //Função que executa verificações se o usuário está de férias ou nao e se ele está perto do período de acumular férias, 
  //retornando um json com os funcionários e o tempo desde sua última solicitação
  try {
    const solicitacoesResponse = await axios.get(`http://127.0.0.1:8000/solicitacoes/`);
    const solicitacoesData = solicitacoesResponse.data;

    const funcionariosResponse = await axios.get(`http://127.0.0.1:8000/funcionario/`);
    const funcionariosData = funcionariosResponse.data;

    const funcionarios = {};

    solicitacoesData.forEach((solicitacao) => {
      if (solicitacao.status === "aprovado") {
        const matricula = solicitacao.matricula_funcionario;
        const ultimasFerias = Math.floor(
          (new Date() - new Date(solicitacao.data_fim)) / (1000 * 60 * 60 * 24 * 30)
        );
        const hoje = new Date();
        const dataInicio = new Date(solicitacao.data_inicio);
        const dataFim = new Date(solicitacao.data_fim);

        if (!funcionarios[matricula]) {
          funcionarios[matricula] = { matricula, ultimasFerias, ferias: "NÃO" };
        } else {
          // Busca a última solicitação aprovada para esse funcionário
          const ultimaSolicitacao = solicitacoesData.filter((s) => s.matricula_funcionario === matricula && s.status === "aprovado")
                                                   .sort((a, b) => new Date(b.data_fim) - new Date(a.data_fim))[0];
          if (ultimaSolicitacao) {
            const ultimasFerias = Math.floor(
              (new Date() - new Date(ultimaSolicitacao.data_fim)) / (1000 * 60 * 60 * 24 * 30)
            );
            funcionarios[matricula].ultimasFerias = ultimasFerias;
          }
        }

        if (hoje >= dataInicio && hoje <= dataFim) {
          funcionarios[matricula].ferias = "SIM";
        }
      }
    });

    funcionariosData.forEach((funcionario) => {
      const matricula = funcionario.matricula;
      if (!funcionarios[matricula]) {
        const hoje = new Date();
        const dataAdmissao = new Date(funcionario.data_admissao);
        const mesesDesdeAdmissao = Math.floor(
          (hoje - dataAdmissao) / (1000 * 60 * 60 * 24 * 30)
        );
        funcionarios[matricula] = { matricula, ultimasFerias: mesesDesdeAdmissao, ferias: "NÃO" };
      }
    });

    const funcionariosArray = Object.values(funcionarios);
    localStorage.setItem("funcionarios", JSON.stringify(funcionariosArray));
  } catch (error) {
    console.log(error);
  }
};




