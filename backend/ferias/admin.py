from django.contrib import admin
from ferias import models

#Registrando as tabelas do banco de dados no Django admin
admin.site.register(models.Funcionario)
admin.site.register(models.Solicitacao)
