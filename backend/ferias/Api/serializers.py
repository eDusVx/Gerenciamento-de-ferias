from rest_framework import serializers
from ferias import models


class FuncionarioSerializer(serializers.ModelSerializer):
    #Serializer da tabela funcionario
    class Meta:
        model = models.Funcionario
        fields = '__all__'

class SolicitacaoSerializer(serializers.ModelSerializer):
    #Serializer da tabela solicitacoes
    nome_funcionario = serializers.CharField(source='matricula_funcionario.nome', read_only=True)
    cargo_funcionario = serializers.CharField(source='matricula_funcionario.cargo', read_only=True)

    def validate_status(self, value):
        choices = ['pendente', 'recusado', 'aprovado']
        if value not in choices:
            raise serializers.ValidationError(f'O valor {value} não é uma opção válida.')
        return value
    
    class Meta:
        model = models.Solicitacao
        fields = '__all__'

from rest_framework_simplejwt.serializers import TokenObtainPairSerializer


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    # Função que adiciona o grupo do usuário ao token
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        token['group'] = user.groups.first().name if user.groups.exists() else None

        return token

    


        