from rest_framework import viewsets,mixins
from rest_framework.views import APIView
from rest_framework.response import Response
from ferias.Api import serializers
from ferias import models
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication
from django.contrib.auth.models import Group
from rest_framework.decorators import action
from datetime import datetime
import calendar
from django.http import HttpResponse
import pandas as pd
from rest_framework.decorators import api_view

from rest_framework_simplejwt.views import TokenObtainPairView
from .serializers import CustomTokenObtainPairSerializer


class CustomTokenObtainPairView(TokenObtainPairView):
    #Define uma viewset para a url que busca o JWT TOKEN
    serializer_class = CustomTokenObtainPairSerializer

class Colaboradores(viewsets.ModelViewSet):
    #Define uma viewset para a url que busca todos os colaboradores
    serializer_class = serializers.FuncionarioSerializer
    queryset = models.Funcionario.objects.all()
    lookup_field = 'matricula'

    def update(self, request, matricula=None):
        funcionario = self.get_object()
        serializer = self.get_serializer(funcionario, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


class Busca_colaboradores(mixins.ListModelMixin, viewsets.GenericViewSet):
    #Define uma viewset para a url que busca todos os colaboradores relacionados a um gestor
    serializer_class = serializers.FuncionarioSerializer

    def get_queryset(self):
        matricula_gestor = self.kwargs.get('matricula_gestor')
        queryset = models.Funcionario.objects.filter(matricula_gestor=matricula_gestor)
        return queryset


class SolicitacaoFuncionarioViewSet(viewsets.ModelViewSet):
    #Define uma viewset para a url que busca todas as solicitações referentes a um funcionarios
    serializer_class = serializers.SolicitacaoSerializer

    def get_queryset(self):
        matricula = self.kwargs['matricula']
        queryset = models.Solicitacao.objects.filter(matricula_funcionario=matricula)
        return queryset

class SolicitacaoViewSet(viewsets.ModelViewSet):
    #Define uma viewset para a url que busca todas as solicitações da tabelea solicitacao
    serializer_class = serializers.SolicitacaoSerializer

    def get_queryset(self):
        queryset = models.Solicitacao.objects.all()
        return queryset

class SolicitacaoGestorViewSet(viewsets.ModelViewSet):
    #Define uma viewset para a url que busca todas as solicitações de funcionarios relacionados a um gestor, podendo editar para responder um agendamento
    serializer_class = serializers.SolicitacaoSerializer
    queryset = models.Solicitacao.objects.all()

    @action(detail=False, methods=['get'], url_path='gestor/(?P<matricula_gestor>[^/.]+)')
    def por_gestor(self, request, matricula_gestor=None):
        if matricula_gestor is not None:
            funcionarios = models.Funcionario.objects.filter(matricula_gestor=matricula_gestor)
            queryset = models.Solicitacao.objects.filter(matricula_funcionario__in=funcionarios)
            serializer = self.get_serializer(queryset, many=True)
            return Response(serializer.data)
        else:
            return Response({"detail": "Matrícula do gestor não informada."}, status=400)


    @action(detail=True, methods=['put'])
    def atualizar_solicitacao(self, request, pk=None):
        try:
            solicitacao = models.Solicitacao.objects.get(pk=pk)
        except models.Solicitacao.DoesNotExist:
            return Response({"detail": "Solicitação não encontrada."}, status=404)

        serializer = self.get_serializer(solicitacao, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data)

class Dashboard1ViewSet(viewsets.ViewSet):
    #Define uma viewset para a url que busca os dados do dashboard1
    def list(self, request, matricula_gestor):
        hoje = datetime.now().date()
        ferias = models.Solicitacao.objects.filter(status='aprovado', data_inicio__lte=hoje, data_fim__gte=hoje, matricula_funcionario__matricula_gestor=matricula_gestor)

        solicitacoes_ativas = models.Solicitacao.objects.filter(status='aprovado', data_fim__gte=hoje, matricula_funcionario__matricula_gestor=matricula_gestor).exclude(id__in=ferias.values_list('id', flat=True))
        funcionarios_ativos = models.Funcionario.objects.filter(matricula_gestor=matricula_gestor).exclude(matricula__in=ferias.values_list('matricula_funcionario__matricula', flat=True))
        ativos = funcionarios_ativos.count()
        em_ferias = ferias.count()

        ativos_list = list(funcionarios_ativos.values_list('nome', flat=True))
        em_ferias_list = list(ferias.values_list('matricula_funcionario__nome', flat=True))

        data = [
            {'colaboradores': 'Ativos', 'quantidade': ativos, 'nomes': ativos_list},
            {'colaboradores': 'Férias', 'quantidade': em_ferias, 'nomes': em_ferias_list},
        ]
        return Response(data)




class Dashboard2ViewSet(viewsets.ViewSet):
    #Define uma viewset para a url que busca os dados do dashboard2
    @action(detail=True, methods=['get'], url_path='matricula_gestor/(?P<matricula_gestor>\d+)')
    def matricula_gestor(self, request, matricula_gestor=None):
        subordinados = models.Funcionario.objects.filter(matricula_gestor=matricula_gestor)
        data = []
        for subordinado in subordinados:
            aprovados = models.Solicitacao.objects.filter(status="aprovado", matricula_funcionario=subordinado).count()
            recusados = models.Solicitacao.objects.filter(status="recusado", matricula_funcionario=subordinado).count()
            pendentes = models.Solicitacao.objects.filter(status="pendente", matricula_funcionario=subordinado).count()

            data.append({
                "nome": subordinado.nome,
                "aprovados": aprovados,
                "recusados": recusados,
                "pendentes": pendentes
            })

        return Response(data)


class Dashboard3ViewSet(viewsets.ViewSet):
    # Define uma viewset para a url que busca os dados do dashboard3
    def list(self, request, matricula_gestor=None, year=None):
        if matricula_gestor:
            if year:
                solicitacoes_aprovadas = models.Solicitacao.objects.filter(status="aprovado", 
                                                                            matricula_funcionario__matricula_gestor__matricula=matricula_gestor, 
                                                                            data_inicio__year=year).select_related('matricula_funcionario')
            else:
                solicitacoes_aprovadas = models.Solicitacao.objects.filter(status="aprovado", 
                                                                            matricula_funcionario__matricula_gestor__matricula=matricula_gestor).select_related('matricula_funcionario')
        else:
            if year:
                solicitacoes_aprovadas = models.Solicitacao.objects.filter(status="aprovado", 
                                                                            data_inicio__year=year).select_related('matricula_funcionario')
            else:
                solicitacoes_aprovadas = models.Solicitacao.objects.filter(status="aprovado").select_related('matricula_funcionario')

        colaboradores_por_ano_mes = {}

        for solicitacao in solicitacoes_aprovadas:
            ano = solicitacao.data_inicio.year
            mes = datetime.strftime(solicitacao.data_inicio, "%b")

            if ano not in colaboradores_por_ano_mes:
                colaboradores_por_ano_mes[ano] = {}
                
            if mes not in colaboradores_por_ano_mes[ano]:
                colaboradores_por_ano_mes[ano][mes] = set()

            colaboradores_por_ano_mes[ano][mes].add(solicitacao.matricula_funcionario.nome)

        response_data = []
        for ano, meses in colaboradores_por_ano_mes.items():
            for mes in calendar.month_abbr[1:]:
                if mes not in meses:
                    meses[mes] = set()

            for mes, colaboradores in sorted(meses.items(), key=lambda x: list(calendar.month_abbr[1:]).index(x[0])):
                response_data.append({
                    "Ano": ano,
                    "Mês": mes,
                    "Colaboradores": len(colaboradores),
                    "Funcionários": list(colaboradores)
                })

        return Response(response_data)



@api_view(['GET'])
def historico_solicitacoes_funcionario(request, matricula):
    #Define uma viewset para a url que retorna um arquivo xlsx com o histórico de solicitações de um funcionário de matricula específica
    queryset = models.Solicitacao.objects.filter(matricula_funcionario=matricula)

    colunas = ['Data', 'Período', 'Status']

    data = []
    for solicitacao in queryset:
        periodo = f"{solicitacao.data_inicio.strftime('%d/%m/%Y')} - {solicitacao.data_fim.strftime('%d/%m/%Y')}"
        data.append([
            solicitacao.data_criacao.strftime('%d/%m/%Y'),
            periodo,
            solicitacao.status
        ])
    df = pd.DataFrame(data, columns=colunas)

    writer = pd.ExcelWriter('historico_solicitacoes_funcionario.xlsx', engine='xlsxwriter')
    df.to_excel(writer, sheet_name='Histórico de Solicitações', index=False)
    worksheet = writer.sheets['Histórico de Solicitações']

    worksheet.set_column('A:D', 30)

    writer.save()

    with open('historico_solicitacoes_funcionario.xlsx', 'rb') as f:
        file_data = f.read()

    response = HttpResponse(file_data, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = f'attachment; filename="historico_solicitacoes_funcionario.xlsx"'

    return response

@api_view(['GET'])
def dashboard_xlsx(request, matricula_gestor):
    #Define uma viewset para a url que retorna um arquivo xlsx com o relatório dos dados dos 3 dashboards.
    dashboard1 = Dashboard1ViewSet().list(request=request, matricula_gestor=matricula_gestor).data
    dashboard2 = Dashboard2ViewSet().matricula_gestor(request=request, matricula_gestor=matricula_gestor).data
    dashboard3 = Dashboard3ViewSet().list(request=request, matricula_gestor=matricula_gestor).data

    df1 = pd.DataFrame(dashboard1)
    df1["nomes"] = df1["nomes"].apply(lambda x: ",".join(x)) # <-- adicione essa linha
    df2 = pd.DataFrame(dashboard2)
    df3 = pd.DataFrame(dashboard3)
    df3["Funcionários"] = df3["Funcionários"].apply(lambda x: ",".join(x))


    writer = pd.ExcelWriter('dashboard_data.xlsx', engine='xlsxwriter')
    df1.to_excel(writer, sheet_name='Colaboradores', index=False)
    df2.to_excel(writer, sheet_name='Solicitações', index=False)
    df3.to_excel(writer, sheet_name='Colaboradores em férias por mês', index=False)

    workbook = writer.book
    dashboard1_worksheet = writer.sheets['Colaboradores']
    dashboard2_worksheet = writer.sheets['Solicitações']
    dashboard3_worksheet = writer.sheets['Colaboradores em férias por mês']
    dashboard1_worksheet.set_column('A:B', 20)
    dashboard2_worksheet.set_column('A:D', 20)
    dashboard3_worksheet.set_column('A:B', 20)
    dashboard3_worksheet.set_column('C:C', 20)

    writer.close()

    with open('dashboard_data.xlsx', 'rb') as f:
        file_data = f.read()

    response = HttpResponse(file_data, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = f'attachment; filename="dashboard_data.xlsx"'

    return response






