from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User,Group
from django.contrib.auth import get_user_model
import requests
import smtplib
from email.mime.text import MIMEText
from django.db.models.signals import post_save
from django.dispatch import receiver
from google.oauth2.credentials import Credentials
from googleapiclient.errors import HttpError
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import os
import datetime
from django.db.models.signals import post_migrate



class Funcionario(models.Model):
    # Definição da tabela Funcionario
    matricula = models.CharField(primary_key=True,max_length=6, null=False)
    nome = models.CharField(max_length=100)
    contrato = models.CharField(max_length=3, choices=[('CLT', 'CLT'), ('PJ', 'PJ')])
    data_admissao = models.DateField()
    email_institucional = models.CharField(max_length=100)
    gmail = models.CharField(max_length=100)
    senha = models.CharField(max_length=40)
    cargo = models.CharField(max_length=40)
    matricula_gestor = models.ForeignKey('ferias.Funcionario',to_field='matricula', on_delete=models.SET_NULL, null=True, related_name='subordinados')


@receiver(post_save, sender=Funcionario)
def criar_usuario(sender, instance, created, **kwargs):
    if created:
        # Criação do usuário
        user = User.objects.create_user(username=instance.matricula, password=instance.senha, email=instance.email_institucional)
        user.first_name = instance.nome.split()[0]
        user.last_name = instance.nome.split()[-1]
        user.save()

        # Adicionando ao grupo de usuário correto
        gestor_1 = Group.objects.get(name='gestor_1')
        gestor_2 = Group.objects.get(name='gestor_2')
        colaborador = Group.objects.get(name='colaborador')

        if instance.matricula_gestor is None:
            gestor_1.user_set.add(user)
        else:
            gestor_1_users = User.objects.filter(groups__name='gestor_1')
            if gestor_1_users.filter(username=instance.matricula_gestor.matricula).exists():
                gestor_2.user_set.add(user)
            else:
                colaborador.user_set.add(user)
    else:
        try:
            user = User.objects.get(username=instance.matricula)
            if user.check_password(instance.senha):
                return
            else:
                user.set_password(instance.senha)
                user.save()
        except User.DoesNotExist:
            pass

@receiver(post_migrate)
def criar_usuarios_faltantes(sender, **kwargs):
    #função que cria os users após rodar o migrate,caso os dados sejam adicionados por inserts no Dbeaver por exemplo.
    funcionarios = Funcionario.objects.all()

    for funcionario in funcionarios:
        if not User.objects.filter(username=funcionario.matricula).exists():
            criar_usuario(sender=None, instance=funcionario, created=True)
            print(funcionario)

class Solicitacao(models.Model):
    # Definição da tabela Solicitacao
    id = models.AutoField(primary_key=True)
    matricula_funcionario = models.ForeignKey(Funcionario, on_delete=models.CASCADE)
    data_inicio = models.DateField()
    data_fim = models.DateField()
    status = models.CharField(max_length=8, choices=[('pendente', 'Pendente'), ('recusado', 'Recusado'), ('aprovado', 'Aprovado')], default='pendente')
    data_criacao = models.DateField()
    antecipacao_salario = models.BooleanField()
    comentarios = models.TextField(null=True)

@receiver(post_save, sender=Solicitacao)
def enviar_solicitacao(sender, instance, created, **kwargs):
    #envio de mensagem via Work Chat e email quando uma solicitação nova é criada
    if created:
        token = "DQVJzbi1raTJheGV6Ump4NE5RYUVUTTZAwNGx5dmdJOE00VTRDUHR6RjVxdTdYOHYtaTcxQW9pVHBhOVhiTW9BQkdrU3A3dUdpOU1EQ0ZAfeU1HaHloSGZAuVGFNQkctSlhMVjJHUHZAMcU82OG90dG5heE9uLW9YSTlOdExBWU8zVnNybm5vMU5kMEFoalBDRUZAvNlkyVG5PSlhqWTEtaWZAyazJ3di0yTTJ1aEdmTG5JMTRTd0tUWFlpb3ctSWFDNndpUWh3OXdn"
        headers = {'Authorization': f'Bearer {token}'}
        data = {
            'recipient': {'id': '100089122876850'},
            'message': {'text': f'Um novo pedido de férias do colaborador {instance.matricula_funcionario.nome} foi solicitado para o período {instance.data_inicio} - {instance.data_fim}'}
        }
        url = "https://graph.facebook.com/v4.0/me/messages"
        response = requests.post(url, headers=headers, json=data)
        
        # Enviar email
        host = '10.0.0.241'
        port = 25
        email_de = 'qqtech-alunos@quero-quero.com.br'
        email_para = ['eduardosvx@gmail.com']

        # Criando mensagem
        email_body = f'Um novo pedido de férias do colaborador {instance.matricula_funcionario.nome} foi solicitado para o período {instance.data_inicio} - {instance.data_fim}.'
        msg = MIMEText(email_body, 'html')
        msg['subject'] = f'Nova Solicitação de férias do colaborador {instance.matricula_funcionario.nome}'
        msg['from'] = email_de
        msg['to'] = ', '.join(email_para)

        # Enviando
        s = smtplib.SMTP(host, port)
        s.ehlo()
        s.sendmail(email_de, email_para, msg.as_string())
        s.quit()

SCOPES = ['https://www.googleapis.com/auth/calendar.readonly',
          'https://www.googleapis.com/auth/calendar',
          'https://www.googleapis.com/auth/calendar.events',
          'https://www.googleapis.com/auth/calendar.events.readonly']

@receiver(post_save, sender=Solicitacao)
def responder_solicitacao(sender, instance, update_fields=None, **kwargs):
    #envio de mensagem via Work Chat,email e marcação no google caledar quando uma solicitação é respondida.
    if not kwargs.get('created', False):
        if update_fields is None or ('status' in update_fields):
            token = "DQVJzbi1raTJheGV6Ump4NE5RYUVUTTZAwNGx5dmdJOE00VTRDUHR6RjVxdTdYOHYtaTcxQW9pVHBhOVhiTW9BQkdrU3A3dUdpOU1EQ0ZAfeU1HaHloSGZAuVGFNQkctSlhMVjJHUHZAMcU82OG90dG5heE9uLW9YSTlOdExBWU8zVnNybm5vMU5kMEFoalBDRUZAvNlkyVG5PSlhqWTEtaWZAyazJ3di0yTTJ1aEdmTG5JMTRTd0tUWFlpb3ctSWFDNndpUWh3OXdn"
            headers = {'Authorization': f'Bearer {token}'}
            data = {
                'recipient': {'id': '100089122876850'},
                'message': {'text': f'Seu pedido de férias para o periodo {instance.data_inicio} - {instance.data_fim} foi {instance.status},{instance.comentarios}'}
            }
            url = "https://graph.facebook.com/v4.0/me/messages"
            response = requests.post(url, headers=headers, json=data)
        
            # Enviar email
            host = '10.0.0.241'
            port = 25
            email_de = 'qqtech-alunos@quero-quero.com.br'
            email_para = ['eduardosvx@gmail.com']

            # Criando mensagem
            email_body = f'Seu pedido de férias para o periodo {instance.data_inicio} - {instance.data_fim} foi {instance.status},{instance.comentarios}'
            msg = MIMEText(email_body, 'html')
            msg['subject'] = f'Resposta de solicitação de férias do periodo {instance.data_inicio} - {instance.data_fim}.'
            msg['from'] = email_de
            msg['to'] = ', '.join(email_para)

            # Enviando
            s = smtplib.SMTP(host, port)
            s.ehlo()
            s.sendmail(email_de, email_para, msg.as_string())
            s.quit()

            inicio = datetime.datetime.strptime(instance.data_inicio.strftime('%Y-%m-%d'), '%Y-%m-%d')
            fim = datetime.datetime.strptime(instance.data_fim.strftime('%Y-%m-%d'), '%Y-%m-%d')

            inicio = inicio.replace(hour=0, minute=0, second=0)
            fim = fim.replace(hour=23, minute=59, second=59)

            creds = None
            if os.path.exists('token.json'):
                creds = Credentials.from_authorized_user_file(os.path.join(os.path.dirname(__file__), 'token.json'), SCOPES)
            if not creds or not creds.valid:
                if creds and creds.expired and creds.refresh_token:
                    creds.refresh(Request())
                else:
                    flow = InstalledAppFlow.from_client_secrets_file(os.path.join(os.path.dirname(__file__), 'credentials.json'), SCOPES)
                    creds = flow.run_local_server(port=0)
                with open('token.json', 'w') as token:
                    token.write(creds.to_json())

            try:
                service = build('calendar', 'v3', credentials=creds)
                event = {
                    'summary': 'Férias',
                    'location': 'Em casa',
                    'description': 'Período de férias',
                    'start': {
                        'dateTime': inicio.isoformat(),
                        'timeZone': 'America/Sao_Paulo',
                    },
                    'end': {
                        'dateTime': fim.isoformat(),
                        'timeZone': 'America/Sao_Paulo',
                    },
                    'reminders': {
                        'useDefault': True,
                    },
                }

                event = service.events().insert(calendarId='primary', body=event).execute()
                print ('Compromisso criado: %s' % (event.get('htmlLink')))

            except HttpError as error:
                print('An error occurred: %s' % error)












