"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from ferias.Api import viewsets
from rest_framework_simplejwt import views as jwt_views


route = routers.DefaultRouter()

route.register(r'funcionario',viewsets.Colaboradores, basename="funcionario")
route.register(r'funcionario',viewsets.Busca_colaboradores, basename="funcionario_relacionado")
route.register(r'solicitacoes', viewsets.SolicitacaoGestorViewSet, basename='solicitacao')
route.register(r'busca', viewsets.Colaboradores)
route.register(r'dashboard3', viewsets.Dashboard3ViewSet, basename='dashboard3')


urlpatterns = [
    path('admin/', admin.site.urls),#Rota para o painel de admin django
    path('token/', viewsets.CustomTokenObtainPairView.as_view(), name ='token_obtain_pair'),#Rota para obter token jwt 
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name ='token_refresh'),#Rota para o refresh token jwt
    path('busca/<str:matricula>', viewsets.Colaboradores.as_view({'get': 'retrieve', 'put': 'update'})),#Rota para buscar as informações de um funcionario específico da tabela funcionarios
    path('dashboard1/<str:matricula_gestor>/', viewsets.Dashboard1ViewSet.as_view({'get': 'list'}), name='dashboard1'),#Rota para buscar informações do dashboard1
    path('dashboard2/<str:matricula_gestor>/', viewsets.Dashboard2ViewSet.as_view({'get': 'matricula_gestor'}), name='dashboard2-matricula-gestor'),#Rota para buscar informações do dashboard2
    path('dashboard3/<str:matricula_gestor>/', viewsets.Dashboard3ViewSet.as_view({'get': 'list'}), name='dashboard3-gestor'),#Rota para buscar informações do dashboard3
    path('dashboard/xlsx/<str:matricula_gestor>/', viewsets.dashboard_xlsx),#Rota para gerar relatório xlsx dos dashboards
    path('solicitacoes/funcionario/<str:matricula>/', viewsets.SolicitacaoFuncionarioViewSet.as_view({'get': 'list'})),#Rota para buscar as solicitações de um funcionario específico
    path('solicitacoes/funcionario/<str:matricula>/historico', viewsets.historico_solicitacoes_funcionario),#Rota para gerar arquivo xlsx com histórico de solicitações de um funcionario.
    path('solicitacao/<int:pk>/', viewsets.SolicitacaoGestorViewSet.as_view({'put': 'atualizar_solicitacao'}), name='atualizar_solicitacao'),#Rota para atualizar uma solicitação podendo responder um agendamento
    path('funcionario/<str:matricula_gestor>/', viewsets.Busca_colaboradores.as_view({'get': 'list'}), name='funcionarios-gestor'),#Rota para funcionarios relacionados a um gestor.
    path('solicitacoes/gestor/<str:matricula_gestor>/', viewsets.SolicitacaoGestorViewSet.as_view({'get': 'por_gestor'}), name='solicitacoes-por-gestor'),#Rota para todas as solicitações dos funcionarios ligados a um gestor especifico
    path('', include('authentification.urls')),#incluindo rotas de outros apps django
    path('',include(route.urls))#incluindo rotas padrões do django
]
